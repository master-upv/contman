# global VARIABLES
CM_CFG_DIR="/etc/contman"

# -------------------------------------
# local configuration files management
# -------------------------------------

write_cfg (){
	# $1 nombre del fichero
	# $2 contenido del fichero
	if [ ! -d "$CM_CFG_DIR" ] ; then
		mkdir -p $CM_CFG_DIR
	fi
	echo "$2" > $CM_CFG_DIR/$1
}

read_cfg () {
	if [ -d "$CM_CFG_DIR" ] && [ -r "$CM_CFG_DIR/$1" ] ; then
		cat $CM_CFG_DIR/$1 
	fi
}

# --------------------------------
# master & slaves common functions
# --------------------------------

default_iptables(){
	NET_MODE=$1
	PRIVATE_NETS="$2"
	case $NET_MODE in
		1) 
			# only communication between containers is allowed
			iptables -I FORWARD 1 -i docker_gwbridge -j DROP
			iptables -I INPUT 1 -i docker_gwbridge -j DROP
			;;
		2) 
			# containers are allowed to communicate with de host
			iptables -I FORWARD 1 -i docker_gwbridge -j DROP
			;;
		3|4|5)	
			# every container can reach others (nodes and containers) except Internet
			iptables -I FORWARD 1 -i docker_gwbridge -j DROP
			iptables -I FORWARD 1 -i docker_gwbridge -d $PRIVATE_NETS -j ACCEPT
			;;
		6|7)
			;;
	esac
}
write_join_token(){
	etcd_put "CM_SWARM_TOKEN" "$(docker_swarm_token)"
}

read_join_token(){
	etcd_get "CM_SWARM_TOKEN"
}


add_routes_2nodes(){
	PRIVATE_NETS=""
	for node in $(etcd_ls_nodes) ; do
		node_ip="$(etcd_get_node_ip $node)"
		node_net="$(etcd_get_node_net $node)"
		PRIVATE_NETS="$PRIVATE_NETS,$node_net"
		route_del $node_ip $node_net
		route_add $node_ip $node_net
	done
	echo "$PRIVATE_NETS"
}

# -----------------------
# tests and sanity checks
# -----------------------

running_in_master(){
	if [ -e "$CM_CFG_DIR/master.cfg" ] ; then
		return 0
	fi
	return 1
}

running_in_slave(){
	if [ -e "$CM_CFG_DIR/slave.cfg" ] ; then
		return 0
	fi
	return 1
}

test_environment(){
	rc=0
	echo -n "Testing configuration ... "
	if [ "$CM_CFG_DIR" ] && [ -d "$CM_CFG_DIR" ] ; then
		echo "($CM_CFG_DIR) [OK]"
	else
		rc=1
		echo "FAILED. Configuration directory not found"
	fi

	echo -n "Testing MASTER_IP ... "
	CM_IP_MASTER="$(read_cfg "master_ip.cfg")"
	if is_ip "$CM_IP_MASTER" ; then
		echo "($CM_IP_MASTER) [OK]"
	else
		rc=1
		echo "FAILED"
	fi

	echo -n "Testing node type ... "
	if running_in_master ; then
		echo "(master) [OK]"
	elif running_in_slave ; then
		echo "(slave) [OK]"
	else
		rc=1
		echo "FAILED"
	fi
	return $rc
}

# --------------------------------
# container manipulation functions
# --------------------------------

cont_create(){
# $1 container's name
# $2 ip
	# verify docker installation
	docker_test_environment

	CT_NAME=$1
	NET_NAME=""
	CT_IP=""
	CT_CMD=""

	IMAGE_CONT="alpine:latest"
	FLAGS="-dit --network none"
	if is_ip "$2" ; then
		CT_IP="--ip $2"
	fi

	NET_MODE="$(etcd_get_mode)"
	# NET_MODE 5 || 7 : (containers in master and slaves use the same overlay network)
	#  		    this case requires additional flag "--cap-add NET_ADMIN" to change default gw inside the container
	#		    the network parameters are read from etcd
	if [ $NET_MODE -eq 5 ] || [ $NET_MODE -eq 7 ] ; then
		FLAGS="$FLAGS --cap-add NET_ADMIN"

		NET_NAME="$(etcd_get_master_gw_name)"
		GW_IP="$(etcd_get_master_gw_ip)"
		CT_CMD="ip route add default via $GW_IP"
	else
		# read network configuration from local files
		NET_NAME="$(read_cfg "node_net_name.cfg")"
	fi

	# run container
	docker_run "$CT_NAME" "$IMAGE_CONT" "$FLAGS" sh

	# attach to network and configure if required
	if [ "$NET_NAME" ] ; then
		docker_net_disconnect none $CT_NAME
		docker_net_connect $NET_NAME $CT_NAME $CT_IP
	fi
	if [ "$CT_CMD" ] ; then
		docker_exec "$CT_NAME" $CT_CMD
	fi

}

cont_delete(){
# $1 container name

	# verify docker installation
	docker_test_environment

	docker_rm "$1"
}

# -----------
# daemon mode
# -----------
daemon_is_running(){
	# check for background process
	CM_PID="$(read_cfg "contman.pid")"
	if [ "$CM_PID" ] && ps -p $CM_PID >/dev/null ; then
		return 0
	fi
	return 1
}

daemon_stop(){
	# check for background process
	CM_PID="$(read_cfg "contman.pid")"
	if [ "$CM_PID" ] && ps -p $CM_PID >/dev/null; then
		kill -9 "$CM_PID"
	fi
	
	remove_cfg "contman.pid"
	return 0
}

daemon_start(){
	if daemon_is_running ; then
		return 0
	fi
	CM_PID=$$
	write_cfg "contman.pid" "$CM_PID"
	# write PID 
	while : ; do
		node="$(etcd_wait_nodes)" 
		node_ip="$(etcd_get_node_ip $node)"
		node_net="$(etcd_get_node_net $node)"
		route_del $node_ip $node_net
		route_add $node_ip $node_net
	done
}

