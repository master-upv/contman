DOCKER_CMD="$(which docker)" || true

docker_test_environment(){
	[ "$DOCKER_CMD" ] || die "ERROR: docker command not found. Please install and configure docker before running this script"
	return 0
}

docker_check(){
	#check container by name
	[ "$1" ] || return 1
	$DOCKER_CMD container inspect "$1" >/dev/null 2>/dev/null || return 1
	return 0
}

docker_start(){
		$DOCKER_CMD start "$1"
}

docker_exec(){
	CT_NAME="$1"
	shift
	$DOCKER_CMD exec "$CT_NAME" "$@"
}

docker_net_create(){
	# $1 name
	# $2 subnet
	# $3 driver (optional)
	net_driver=""
	[ -z "$3" ] || net_driver="-d $3"

	$DOCKER_CMD network create $net_driver --subnet="$2" --attachable "$1"
}

docker_swarm_init(){
	$DOCKER_CMD swarm init --advertise-addr=$CM_IP_MASTER > /dev/null

}

docker_swarm_token(){
	# show join token
	$DOCKER_CMD swarm join-token worker |grep "docker swarm join"
}

docker_run(){
	# create (if necessary) and start container
	# $1 CT name
	# $2 CT image
	# $3 CT flags
	# additional parameters will be passed "as is" to docker run command
	CT_NAME="$1"
	if docker_check "$CT_NAME" ; then
		docker_start "$1"
	else
		shift
		CT_IMAGE="$1"
		shift
		CT_FLAGS="$1"
		shift
		$DOCKER_CMD run $CT_FLAGS --name $CT_NAME $CT_IMAGE "$@"
	fi
}

docker_rm(){
	if docker_check "$1" ; then
		$DOCKER_CMD stop "$1" > /dev/null
		$DOCKER_CMD rm "$1" > /dev/null
	fi
}

docker_net_connect(){
	# $1 red
	# $2 container id
	$DOCKER_CMD network connect "$@"
}

docker_net_disconnect(){
	# $1 red
	# $2 container id
	$DOCKER_CMD network disconnect $1 $2
}
