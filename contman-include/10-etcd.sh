which etcdctl > /dev/null || die "ERROR: etcdctl utility not found. Please install 'etcd-client' or equivalent package"

etcd_init(){
	etcdctl -C http://$CM_IP_MASTER:2379 mkdir /contman/ 
	etcdctl -C http://$CM_IP_MASTER:2379 mkdir /contman/master 
	etcdctl -C http://$CM_IP_MASTER:2379 mkdir /contman/nodes 
	etcdctl -C http://$CM_IP_MASTER:2379 mkdir /contman/nodes/master 
}

etcd_get(){
	# $1 key
	 etcdctl -C http://$CM_IP_MASTER:2379 get /contman/$1 
}

etcd_put(){
	# $1 key $2 value
	 etcdctl -C http://$CM_IP_MASTER:2379 set /contman/$1 "$2" >/dev/null
}

etcd_get_mode(){
	etcd_get "master/CM_NETWORK" 
}

etcd_put_mode(){
	etcd_put "master/CM_NETWORK" $1
}

etcd_get_overnet_cont(){
	etcd_get "master/CM_OVERNET_CONT" 
}
etcd_put_overnet_cont(){
	etcd_put "master/CM_OVERNET_CONT" $1
	etcd_put "master/CM_OVERNET_PREFIX" $2
}
etcd_prepare_overnet(){
	CONT=etcd_get "master/CM_OVERNET_CONT" 
	PREFIX=etcd_get "master/CM_OVERNET_PREFIX" 
	CONT=$(($CONT+1))
	etcd_put "master/CM_OVERNET_CONT" $CONT
	echo "$PREFIX.$CONT/24"
}

etcd_add_node(){
	#$1 nombre del nodo
	#$2 IP attachable en la LAN de los HOSTs
	#$3 lista de redes privadas
	etcdctl -C http://$CM_IP_MASTER:2379 mkdir /contman/nodes/$1
	etcd_put nodes/$1/CM_IP $2
	etcd_put nodes/$1/CM_NET $3
}

etcd_wait_nodes(){
	# wait (recursive) for changes in /contman/nodes and return the node path
	UPDATED_KEY="$(etcdctl -C http://$CM_IP_MASTER:237 watch -r  /contman/nodes |grep "\[set\]"  |cut -f 2 -d " ")"
	echo ${UPDATED_KEY%/*}
}

etcd_ls_nodes(){
	etcdctl -C http://$CM_IP_MASTER:2379 ls /contman/nodes/
}

etcd_get_node_ip(){
	etcdctl -C http://$CM_IP_MASTER:2379 get $1/CM_IP 
}

etcd_get_node_net(){
	etcdctl -C http://$CM_IP_MASTER:2379 get $1/CM_NET 
}

etcd_set_master_ip_gw (){
	# $1 master container gw's IP
	# $2 master container gw's network
	# $3 master gw network's name
	etcd_put master/gw $1
	etcd_put master/gw_net $2
	etcd_put master/gw_net_name $3
}

etcd_get_master_gw_ip (){
	etcd_get master/gw
}

etcd_get_master_gw_name (){
	etcd_get master/gw_net_name
}

