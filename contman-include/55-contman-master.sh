init_master(){
	# $1 MASTER_IFACE
	# $2 NETWORK_MODE
	# $3 CONT_NETWORK

	MASTER_IFACE="$1"
	NET_MODE="$2"
	CONT_NETWORK="$3"
        write_cfg "master_ip.cfg" "$CM_IP_MASTER"

	# create master config file
	write_cfg "master.cfg" "$CM_IP_MASTER"

        # create etcd container
	docker_run "CM_MASTER_ETCD" "quay.io/coreos/etcd:v2.3.8" "-d -v /usr/share/ca-certificates/:/etc/ssl/certs -p 4001:4001 -p 2380:2380 -p 2379:2379" -name cm_master_etcd -advertise-client-urls http://${CM_IP_MASTER}:2379,http://${CM_IP_MASTER}:4001 -listen-client-urls http://0.0.0.0:2379,http://0.0.0.0:4001 -initial-advertise-peer-urls http://${CM_IP_MASTER}:2380 -listen-peer-urls http://0.0.0.0:2380 -initial-cluster-token etcd-cluster-1 -initial-cluster cm_master_etcd=http://${CM_IP_MASTER}:2380 -initial-cluster-state new

	# init swarm and write token in etcd
	docker_swarm_init
	write_join_token

	PRIVATE_NETS="$(get_iface_net $MASTER_IFACE)"
	# write network mode in etcd
	etcd_put_mode "$NET_MODE"

	# create overlay networks
	bytetmp="${CONT_NETWORK%.*}"
	byte12="${bytetmp%.*}"
	byte3="${bytetmp##*.}"
	etcd_put_overnet_cont $byte3 $byte12
	for i in $(seq $byte3 255) ; do
		net_address="${byte12}.${i}.0/24"
		docker_net_create CMNET$i "$net_address" overlay
		PRIVATE_NETS="$PRIVATE_NETS,$net_address"
	done

	# master node uses the first overlay network
	MASTER_NETWORK="${byte12}.${byte3}.0/24"
	MASTER_NETNAME=CMNET$byte3

	# register as node in etcd. Other nodes will read this information to update routing tables
	etcd_add_node "MASTER" "$CM_IP_MASTER" "$MASTER_NETWORK"

        # NET_MODE 5 || 7 : containers in master and slaves use the same overlay network.
        #                   master node provides an "special" docker to act as common gateway for all containers in the overlay (irrespective of running in master or slave)
	#		    The gateway container name is "CM_MASTER_GW" and is connected to 2 networks:
        #		     - the common overlay and 
	#		     - a bridge "CM_MASTER_BRIDGE" in the master host to get acces to internet (if required)
	
	if [ $NET_MODE -eq 5 ] || [ $NET_MODE -eq 7 ] ; then
		# create container gateway
		IMAGE_NAME="alpine:latest"
		FLAGS="-dit --network none --cap-add NET_ADMIN"
		MASTER_GW_NAME=CM_MASTER_GW
		docker_run "$MASTER_GW_NAME" "$IMAGE_NAME" "$FLAGS" sh
		
		MASTER_GW_IP=$byte12.$byte3.254
		MASTER_BRIDGE_NET="172.20.0.0./24"
		MASTER_BRIDGE_IP="172.20.0.254"
		MASTER_BRIDGE_HOST="172.20.0.1"

		# create bridge and configure container's networking
		docker_net_create CM_MASTER_BRIDGE $MASTER_BRIDGE_NET 

		docker_net_disconnect none $MASTER_GW_NAME 
		docker_net_connect $MASTER_NETNAME $MASTER_GW_NAME --ip $MASTER_GW_IP
		docker_net_connect CM_MASTER_BRIDGE $MASTER_GW_NAME --ip $MASTER_BRIDGE_IP

		# add route in master host to access overlay network via CM_MASTER_GW bridge ip address
		route_add $MASTER_BRIDGE_IP $MASTER_NETWORK

		# change default gw in the container
		CMD_ADD_GW="ip route add default via $MASTER_BRIDGE_HOST"
		docker -exec $MASTER_GW_NAME $CMD_ADD_GW

		# write required information in etcd 
		etcd_set_master_ip_gw $MASTER_GW_IP $MASTER_NETWORK $MASTER_NETNAME
	elif [ $NET_MODE -ge 2 ] ; then
		# add a route over docker_gwbridge to access containers from host (NET_MODE >=2 except special cases 5 & 7)
		route_add docker_gwbridge $MASTER_NETWORK
	fi

	# NET_MODE 7: add nat rules to get Internet access
	if [ $NET_MODE -eq 7 ] ; then
		HOST_NET="$(get_iface_ip $MASTER_IFACE)"
		iptables -t nat -A POSTROUTING -s $OVERNET ! -d $HOST_NET ! -o docker_gwbridge -j MASQUERADE
	fi
	if [ $NET_MODE -ne 1 ] ; then
		# write local files with master network configuration for docker creation (just like slaves do)
		write_cfg "node_id.cfg" "MASTER"
		write_cfg "node_net_address.cfg" "$MASTER_NETWORK"
		write_cfg "node_net_name.cfg" "$MASTER_NETNAME"
	fi

	default_iptables $NET_MODE $PRIVATE_NETS

}
