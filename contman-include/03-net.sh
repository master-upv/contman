IP_CMD="$(which ip)" || die "ERROR: ip utility not found. Please install 'iproute2' or equivalent package"

route_del(){
	# borrar las rutas de encaminamiento las redes de contenedores privadas de los slaves
	#$1 gw
	#$2 network to delete 
	LANG=C $IP_CMD ro del $2 via $1 |cut -d "/" -f 1 2> /dev/null || true
}


is_ip(){
	# very simple test. More work needed ....
	echo "$1" |grep -q "^[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}$" || return 1
	return 0
}

route_add(){
	# añadir las rutas de encaminamiento
	#$1 IP: GW or DEVICE 
	#$2 network to add 
	if is_ip $1 ; then 
		LANG=C $IP_CMD ro add $2 via $1
	else 
		LANG=C $IP_CMD ro add $2 dev $1
	fi
}

is_cidr(){
	ADDR="${1%/*}"
	if [ "$ADDR" ] && is_ip "$ADDR" ; then
		MASK="${1#*/}"
		if echo "$MASK" |grep -q "^[0-9]\{1,2\}$" ; then
			return 0
		fi
	fi
	return 1
}

iface_list(){
	LANG=C $IP_CMD -o -4 a show |tr " " "\t" |tr -s "\t" |cut -f 2
}

is_iface(){
	[ "$1" ] || return 1
	iface_list |grep -q "^${1}$" || return 1
	return 0
}

get_iface_ip(){
	if is_iface "$1" ;  then
		LANG=C $IP_CMD -o -4 a show dev "$1" |tr " " "\t" |tr -s "\t" |cut -f 4 |cut -d "/" -f 1
		return 0
	fi
	return 1
}

get_iface_net(){
	if is_iface "$1" ;  then
		LANG=C $IP_CMD -o -4 route show dev "$1" |grep "kernel scope" |cut -f 1 -d " "
		return 0
	fi
	return 1
}

		

