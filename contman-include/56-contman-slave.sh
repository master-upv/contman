init_slave(){
	# $1 SLAVE_IFACE
	SLAVE_IFACE="$1"
	write_cfg "master_ip.cfg" "$CM_IP_MASTER"

	# crea fichero de configuración del slave
	write_cfg "slave.cfg" "$CM_IP_MASTER"

	# lee el modo de fucnionamiento desde etcd
	NET_MODE="$(etcd_get_mode)"
	if [ ! "$NET_MODE" ] ; then 
		die "ERROR: Scene isn't specified."
	fi

	# NET_MODE >= 2 : hay comunicacion entre los host de la LAN y los contenedores
	PRIVATE_NETS=""
	if [ $NET_MODE -ge 2 ] ; then
		# encuentra todos las redes de contenedores en otros hosts
		# y añade las reglas de enrutamiento a través de dichos hosts
		PRIVATE_NETS="$(add_routes_2nodes)" 
	fi

	if [ "$SLAVE_IFACE" ] ; then
		# si se ha especificado la interfaz (y el escenario lo permite) se configura el nodo para que pueda lanzar contenedores

		# NET_MODE > 3 : se pueden desplegar contenedores en hosts diferentes
		if [ $NET_MODE -gt 3 ] ; then
			CMD_JOIN="$(read_join_token)"
			$CMD_JOIN
		fi

		# NET_MODE 4 : Cada host despliega los contenedores en redes diferentes
		#              Las redes de contenedores se comunican a través de los respectivos hosts
		if [ $NET_MODE -eq 4 ] || [ $NET_MODE -eq 6 ] ; then
			# Nos asignamos una de las redes overlay para desplegar nuestros contenedores
			OVERNET="$(etcd_prepare_overnet)"
			OVERNET_CONT="$(etcd_get_overnet_cont)"

			SLAVE_IP="$(get_iface_ip $SLAVE_IFACE)"
			# Escribimos en etcd la información para que se puedan crear reglas de enrutamiento
			# a través de este host hasta nuestra red de contenedores
			etcd_add_node "SLAVE-$OVERNET_CONT" "$SLAVE_IP" "$OVERNET"

			# Añadimos una ruta a través del dispositivo docker_gwbridge
			# para acceder al overlay
			route_add docker_gwbridge $OVERNET	

			# escribimos localmente la información para configurar la red cuando se deplieguen contenedores
			write_cfg "node_id.cfg" "SLAVE-$OVERNET_CONT"
			write_cfg "node_net_address.cfg" "$OVERNET"
			write_cfg "node_net_name.cfg" "CMNET$OVERNET_CONT"

		fi

		# NET_MODE 5 || 7 : Todos los hosts despliegan los contenedores en la misma red overlay
		#                   El nodo master actua como gateway común para todo el overlay
		#		    No se necesitan más reglas, basta con el enrutamiento hasta el nodo master
		#		    que ya se ha establecido (redes en "$PRIVATE_NETS")


		# NET MODE 6 Escenario en el que se necesita salir a Internet: se requiere añadir iptable nat
		HOST_NET="$(get_iface_net $SLAVE_IFACE)"
		if [ $NET_MODE -eq 6 ] ; then
			if [ ! "$PRIVATE_NETS" ] ; then 
				PRIVATE_NETS="$PRIVATE_NETS,$HOST_NET"
			else
				PRIVATE_NETS=$HOST_NET
			fi
			iptables -t nat -A POSTROUTING -s $OVERNET ! -d $PRIVATE_NETS ! -o docker_gwbridge -j MASQUERADE
		fi
		iptables_default $NET_MODE $PRIVATE_NETS 
	fi			
}
