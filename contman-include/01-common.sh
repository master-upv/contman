die(){
	echo "$1" >&2
	exit 1
}

user_exists(){
	getent passwd "$1" >/dev/null || return 1
	return 0
}

get_user_home(){
	getent passwd "$1" |cut -d : -f 6
}

running_as_root(){
	[ $(id -u) -eq 0 ] || return 1
	return 0
}

is_alnum(){
	echo "$1" |grep -q "^[[:alnum:]]\+$" || return 1
	return 0
}

is_num(){
	echo "$1" |grep -q "^[0-9]\+$" || return 1
	return 0
}

