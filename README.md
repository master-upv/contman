Resumen: 

El programa permite resolver los diferentes escenarios que se plantean en la práctica mediante la configuración de un nodo principal (“master”) donde se crean los contenedores. 

En los escenarios en los que intervienen otros hosts, el nodo master proporciona las facilidades de enrutamiento al resto de la red, y almacena centralizadamente toda la información y parámetros necesarios para dicho propósito.  

La configuración del resto de nodos (“slaves”) se realiza desde cada host mediante el mismo script, pero la centralización de la configuración simplifica la inicialización de manera que para inicializar un nodo slave únicamente es necesario proporcionar la dirección IP del nodo master (aunque en los escenarios en los que el slave también puede tener contenedores se requiere el nombre de la interfaz de red que conecta con el master) 

Para las redes de contenedores se utiliza el driver “overlay”. Aunque varios de los escenarios pueden resolverse con otros modelos como “bridge”, consideramos que overlay proporciona la mejor independencia del host (ni siquiera tiene una interfaz con IP en esta red). Con overlay hemos podido concentrarnos en escribir código para permitir exclusivamente lo que se necesita en cada escenario (a diferencia de otros drivers, en los que por defecto se proporcionan más funcionalidades de las necesarias). Además, este driver es la única opción que hemos encontrado para resolver el escenario 3, apartado 5 y el escenario 4, apartado 7. 

El uso de redes overlay, exige la creación de un “swarm”. Todos los hosts que despliegan contenedores se unen al swarm. El master se configura como manager mientras que el resto de hosts lo hacen como workers. 

No obstante, la creación de overlays conlleva la aparición de un nuevo bridge “docker_gwbridge” que permite la conexión entre los hosts y los contenedores de la red overlay. Docker añade reglas de iptables por defecto sobre este bridge que hemos tenido que tener en cuenta para restringir la conectividad a los diferentes casos del escenario. 

Requisitos y dependencias software: 

El programa se ha realizado en shell script y prácticamente no requiere tener instalados paquetes adicionales más allá de utilidades esenciales de propósito general (sed, grep, etc) disponibles habitualmente en cualquier sistema linux. 

Se utiliza “etcd” para mantener centralizados los datos de configuración. Los hosts utilizan el cliente (etcdctl) para recuperar inofrmación, por lo que es necesario tenerlo instalado en todos los hosts participantes (paquete “etcd-client” en sistemas Debian/Ubuntu). 

No hace falta instalar el servidor en ningún host, ya que el nodo master despliega un docker con una imagen que contiene al servidor, tal y como está documentado en la propia web de etcd (https://etcd.io/docs/v2.3/docker_guide/). Este docker se llama “CM_MASTER_ETCD“ y utiliza “host” como driver de red, de forma que el servicio parece ofrecerse directamente desde la IP del nodo master. 

Los requisitos de los escenarios con múltiples nodos exigen establecer reglas de enrutamiento. Para la configuración de la red se utiliza el comando “ip” , por lo que también deberá estar instalado en todos los hosts que intervengan (paquete “iproute2” en sistemas Debian/Ubuntu). 

Tanto en el nodo master como en los slaves en los que el escenario requiera que se desplieguen contenedores, debe estar instalado y configurado docker.  En general se necesitará tener acceso a Internet desde dichos nodos para poder descargar imágenes de dockerhub. También necesitan tener instalado el comando “iptables” para poder configurar reglas de cortafuegos. 

Por último, deberá descargarse “contman” también en todos los nodos participantes. 

Requisitos funcionales: 

Para poder configurar el enrutamiento, la ejecución del script requiere privilegios de administrador en todos los nodos. Para este proceso, se tendrá que ejecutar el script en cada uno de los nodos. 

Será imprescindible inicializar en primer lugar el nodo master. 

Para la inicialización del nodo master se debe proporcionar: 

La interfaz de red que se utilizará para comunicarse con el resto de hosts de la LAN. A partir del nombre de la interfaz, el script detecta los parámetros necesarios de la red a la que está conectada. 

El “modo de red”. Se trata de parametrizar el tipo de escenario mediante un número del 1 al 7 que se corresponde con los apartados de los escenarios del enunciado de la práctica. 

La dirección de la red de contenedores (opcional). Por defecto se utiliza “192.168.111.0/24”. Sólo se admite “24” como máscara de red. En los escenarios con múltiples redes de contenedores, se crearán otras redes. Si no se especifica una dirección diferente, las redes se crearán incrementando el tercer octeto de la red de contenedores del master, lo que establece una limitación al número de hosts en dichos escenarios. 

Para la inicialización de un nodo slave se debe proporcionar: 

La dirección IP del nodo master, obligatoriamente. 

Si queremos que se puedan lanzar contenedores en el slave (en los escenarios compatibles), también será necesario especificar la interfaz de red con al que el slave se comunica con el master. En este caso también se detectan automáticamente los parámetros de red necesarios.  Si no se especifica esta interfaz, se entiende que el nodo participa sólo para conseguir enrutamiento a los diferentes contenedores. 

Uso del programa: 

Programa principal: “contman” 

Usage: 

contman init-master MASTER_IFACE NETWORK_MODE [CONT_NETWORK] 

contman init-slave MASTER_IP [SLAVE_IFACE] 

contman create CONTAINER_NAME [CONTAINER_IP] 

contman delete CONTAINER_NAME 

contman daemon start 

contman daemon stop 

 

Además de las opciones de inicialización del nodo, tenemos: 

create: para crear un contenedor. Opcionalmente se puede indicar su IP 

delete: para borrar un contenedor 

daemon (start|stop): se ha incluido la posibilidad de lanzar el programa (en un nodo previamente inicializado) de manera que se quede a la espera de la incorporación de nuevos nodos, a fin de actualizar su tabla de enrutamiento para acceder a las nuevas redes de contenedores que puedan aparecer. 

Modos de funcionamiento en los diferentes escenarios; 

La conectividad de los docker con el resto de la red se realiza mediante el bridge “docker_gwbridge” del host en el que se están ejecutando (excepto en los modos 5 y 7). Para ello se añade una regla de enrutamiento en el host hacia la red overlay directamente a través de dicha interfaz, y las reglas de iptables necesarias para limitar la conectividad. 

En el modo 4, se permiten varios hosts con contenedores. A cada uno de ellos se le asigna un overlay diferente con lo que cada red docker utiliza un direccionamiento diferente. Estas redes se crean al inicializar el nodo master (ya que es el único manager del swarm). Los nombres de estas redes en docker son “CMNET” y el tercer octeto de la red (por defecto, CMNET111, CMNET112, …). 

En los modos 5 y 7 se utiliza la misma red overlay en todos hosts que desplieguen contenedores. Es la red del nodo master (CMNET111 por defecto). En este caso, la estrategia de añadir una ruta en el host hasta el overlay únicamente nos permitía acceder a los contenedores que se ejecutan en el propio host. Para resolver la conectivida se ha optado por despllegar un contenedor en el master (CM_MASTER_GW) con dos interfaces de red, el overlay y un nuevo bridge en el anfitrión (CM_MASTER_BRIDGE). Este contenedor actúa como gateway por defecto para todos los contenedores del overlay, con lo que el nodo master se convierte en el punto de entrada para el acceso a todo el overlay de contenedores. 

En los escenarios con internet (6 y 7) se añaden reglas de nat. 

Organización del código 

Para estructurar mejor el código y poder trabajar concurrentemente, el programa se ha divido en diferentes ficheros, que se cargan desde el script principal “contman” a modo de librerías. El programa principal contiene sólo la gestión y comprobación de los parámetros suministrados y después llama a las diferentes funciones. 

Los ficheros están en el directorio contman-include. Y se han organizado por funcionalidad 

01-common.sh : funciones de propósito general 

03-net.sh : funciones generales para el acceso/configuración de parámetros de red 

05-docker.sh : encapsula la ejecución de servicios docker 

10-etcd.sh : acceso al servicio etcd 

50-contman.sh : funciones de uso común para el nodo master y los slaves 

55-contman-master.sh : inicialización del nodo master 

56-contman-slave.sh : inicialización de un nodo esclavo 


